FROM ruby:2.7-alpine

WORKDIR /mr
ENV PATH="/mr:$PATH"

COPY Gemfile* /mr/
RUN apk add git build-base && \
    gem update bundler && \
    bundle install --jobs "$(nproc)" --retry 2  && \
    apk del build-base

COPY . ./

CMD ["/bin/sh", "-c", "mr-prompt"]
