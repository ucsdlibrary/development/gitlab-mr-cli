# Why

Some of us prefer the terminal to the GitLab UI for creating merge requests

NOTE: THIS IS A WIP blocked currently by https://gitlab.com/gitlab-org/gitlab/-/issues/241710

# How

Clone the repo:

```
git clone git@gitlab.com:ucsdlibrary/development/gitlab-mr-cli.git
```

Build the docker image:

```
docker build -t gitlab-merge-request .
```

Put the `mr-create` script in your `PATH`

Create an MR when you're ready to push changes in a project:

```
cd <your project>
<make changes>
<git add>
<git commit>
mr-create
```

